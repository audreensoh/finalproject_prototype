﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using WatchMe.DAL;
using Microsoft.AspNet.Identity;
using System;

namespace WatchMe.Hubs
{
    [Authorize]
    public class NotificationHub : Hub
    {
        HashSet<string> ConnectedIds = UserHandler.ConnectedIds;
        BinoclrDBEntities db = new BinoclrDBEntities();
        public async void FetchNotification(string userId)
        {
            
            var notification = db.Notifications.Where(n => n.Receiver == userId);
            //context.Clients.All.displayNotification(msg);
            List<Notification> list = notification.ToList();
        
            for (int i = 0; i < list.Count; i++)
            {
                User u = await db.Users.FindAsync(list[i].Sender);
                // Clients.User(userId).addNewNotificationToPage(u.UserName, u.Id);
                string respond;
                switch (list[i].Type)
                {
                    case 1:
                        respond = u.UserName + " followed you.";
                        Clients.User(userId).addNewNotificationToPage(respond, u.Id);
                        break;
                    case 2:
                        respond = u.UserName + " requested to follow you.";
                        Clients.User(userId).addNewNotificationToPage(respond, u.Id);
                        break;
                    case 3:
                        break;

                }

            }

        }

        public async Task Static_FetchNotification(string userId)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var notification = db.Notifications.Where(n => n.Receiver == userId);

            List<Notification> list = notification.ToList();


            for (int i = 0; i < list.Count; i++)
            {
                User u = await db.Users.FindAsync(list[i].Sender);
                string respond="";
                switch (list[i].Type)
                {
                    case 1:
                        respond = u.UserName + " followed you.";                       
                        break;
                    case 2:
                        respond = u.UserName + " requested to follow you.";
                        break;
                    case 3:
                        break;

                }

                context.Clients.User(userId).addNewNotificationToPage(respond, u.Id);
            }

        }

        public override Task OnConnected()
        {
            UserHandler.ConnectedIds.Add(Context.User.Identity.GetUserId());
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            UserHandler.ConnectedIds.Remove(Context.User.Identity.GetUserId());
            return base.OnDisconnected(stopCalled);
        }
    }


    public class CustomUserIdProvider : IUserIdProvider
    {

        public string GetUserId(IRequest request)
        {
            BinoclrDBEntities db = new BinoclrDBEntities();
            var user = new User();
            if (request.User.Identity.Name != null)
            {
                try
                {
                    user = (User)db.Users.FirstOrDefault(u => u.UserName == request.User.Identity.Name);
                }
                catch(Exception e)
                {
                    return null;
                }
            }
            return user.Id;
        }
    }

    public static class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }

}

