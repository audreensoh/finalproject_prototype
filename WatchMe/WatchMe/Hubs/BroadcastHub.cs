﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using WatchMe.DAL;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;


namespace WatchMe.Hubs
{
    [Authorize]
    public class BroadcastHub : Hub
    {
        public static Dictionary<string, string> liveBroadcast = new Dictionary<string, string>();
        public void sendBroadcastToFollower(string broadcasterId,string connection)
        {
            //List<string> loggedOnUsers = HttpRuntime.Cache["LoggedInUsers"] as List<string> ;
            Dictionary<string, DateTime> loggedOnUsers = MvcApplication.SecurityHelper.GetLoggedInUsers();

            Dictionary<string, DateTime> online = loggedOnUsers;
            online.Remove(broadcasterId);
            if (liveBroadcast.ContainsKey(broadcasterId))
            {
                liveBroadcast.Remove(broadcasterId);
            }
            liveBroadcast.Add(broadcasterId, connection);

            BinoclrDBEntities db = new BinoclrDBEntities();
            
            User user = db.Users.FirstOrDefault(u => u.Id == broadcasterId);
            List<Follower> followers = user.Followers.ToList();
            for (int i = 0; i < followers.Count; i++)
            {
                for (int j = 0; j < online.Count; j++)
                {
                    if (online.ElementAt(j).Key == followers.ElementAt(i).FollowerId)
                    {
                        Clients.User(followers.ElementAt(i).FollowerId).receive(broadcasterId,user.UserName,connection);

                    }
                }

            }
        }

        public void sendAcceptToBroadcaster(string broadcasterId,string currentUserId, string connection)
        {

            if (liveBroadcast.ContainsKey(broadcasterId))
            {
                Clients.User(broadcasterId).receive(currentUserId,"", connection);
            }

        }

        public void getLiveBroadcast(string id)
        {
            Dictionary<string, DateTime> loggedOnUsers = MvcApplication.SecurityHelper.GetLoggedInUsers();
            Dictionary<string, DateTime> online = loggedOnUsers;
            BinoclrDBEntities db = new BinoclrDBEntities();


            for (int k = 0; k < liveBroadcast.Count; k++)
            {
                User user = db.Users.FirstOrDefault(u => u.Id == liveBroadcast.ElementAt(k).Key);
                List<Follower> followers = user.Followers.ToList();
                for (int i = 0; i < user.Followers1.Count; i++)
                {
                    for (int j = 0; j < online.Count; j++)
                    {
                        if (online.ElementAt(j).Key== id && online.ElementAt(j).Key == followers.ElementAt(i).FollowerId)
                        {
                            Clients.User(id).receive(user.UserName, liveBroadcast.ElementAt(k).Value);

                        }
                    }
                }
            }
        }

        public void sendIceToFollower(string broadcasterId, string connection)
        {
            //List<string> loggedOnUsers = HttpRuntime.Cache["LoggedInUsers"] as List<string> ;
            Dictionary<string, DateTime> loggedOnUsers = MvcApplication.SecurityHelper.GetLoggedInUsers();

            Dictionary<string, DateTime> online = loggedOnUsers;
            online.Remove(broadcasterId);

            BinoclrDBEntities db = new BinoclrDBEntities();

            User user = db.Users.FirstOrDefault(u => u.Id == broadcasterId);
            List<Follower> followers = user.Followers.ToList();
            for (int i = 0; i < user.Followers1.Count; i++)
            {
                for (int j = 0; j < online.Count; j++)
                {
                    if (online.ElementAt(j).Key == followers.ElementAt(i).FollowerId)
                    {
                        Clients.User(followers.ElementAt(i).FollowerId).receive(broadcasterId, user.UserName, connection);

                    }
                }

            }
        }

        public void sendIceToBroadcaster(string broadcasterId, string connection)
        {
            if (broadcasterId!=null && liveBroadcast.ContainsKey(broadcasterId))
            {
                Clients.User(broadcasterId).receive(broadcasterId,"", connection);
            }
        }


        public override Task OnConnected()
        {
            BroadcastHandler.ConnectedIds.Add(Context.User.Identity.GetUserId());
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            BroadcastHandler.ConnectedIds.Remove(Context.User.Identity.GetUserId());
            for (int j = 0; j < liveBroadcast.Count; j++)
            {
                if (liveBroadcast.ElementAt(j).Key == Context.User.Identity.GetUserId())
                {
                    liveBroadcast.Remove(Context.User.Identity.GetUserId());
                }

            }
            return base.OnDisconnected(stopCalled);
        }

        public static class BroadcastHandler
        {
            public static HashSet<string> ConnectedIds = new HashSet<string>();
        }
    }


}