﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WatchMe.Controllers
{
    [Authorize]
    public class BroadcastController : Controller
    {
        // GET: Broadcast
        public ActionResult Broadcast()
        {
            return View();
        }

        // GET: Broadcast/Details/5
        public ActionResult WatchBroadcast(string connection)
        {
            return View();
        }

    }
}
