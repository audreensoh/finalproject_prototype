﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WatchMe.DAL;
using Microsoft.AspNet.Identity;
using WatchMe.Hubs;

namespace WatchMe.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        private BinoclrDBEntities db = new BinoclrDBEntities();

        // GET: Search
        public async Task<ActionResult> Index(string searchString)
        {
            //var users = db.Users.ToListAsync();
            var users = from u in db.Users select u;
            string userId = User.Identity.GetUserId();
            users = users.Where(x => x.Id != userId);
            users = users.OrderBy(u => u.UserName);

            //foreach (var u in users)
            //{
            //    string id = u.Id;
            //    if (db.Followings.Any(f => f.UserId == User.Identity.GetUserId() && f.FollowingId == id))
            //    {

            //    }

            //}

            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(u => u.UserName.Contains(searchString)
                                       || u.Email.Contains(searchString));
            }

            ViewBag.Message = User.Identity.GetUserId();


            return View(await users.ToListAsync());
        }


        public async Task<ActionResult> Follow(string id)
        {
            User user = await db.Users.FindAsync(id);
            if (user != null)
            {
                if (user.Private == false)
                {
                    Follower follower = new Follower();
                    follower.UserId = user.Id;
                    follower.FollowerId = User.Identity.GetUserId();
                    db.Followers.Add(follower);


                    Following following = new Following();
                    following.UserId = User.Identity.GetUserId();
                    following.FollowingId = user.Id;
                    db.Followings.Add(following);


                    Notification noti = new Notification();
                    noti.Sender = User.Identity.GetUserId();
                    noti.Receiver = id;
                    //1= send followed by XX notification
                    noti.Type = 1;
                    db.Notifications.Add(noti);

                }
                else
                {
                    Notification noti = new Notification();
                    noti.Sender = User.Identity.GetUserId();
                    noti.Receiver = id;
                    //2= send a follow request notification
                    noti.Type = 2;
                    db.Notifications.Add(noti);

                }
                await db.SaveChangesAsync();

                NotificationHub hub = new NotificationHub();
                await hub.Static_FetchNotification(id);
            }

            return RedirectToAction("Index");
        }



        public async Task<ActionResult> Unfollow(string id)
        {
            User user = await db.Users.FindAsync(id);
            string currentUserId = User.Identity.GetUserId();
            if (user != null)
            {
                db.Followers.RemoveRange(db.Followers.Where(f=> f.UserId == user.Id && f.FollowerId == currentUserId));
                db.Followings.RemoveRange(db.Followings.Where(f => f.UserId == currentUserId && f.FollowingId == user.Id));
            }
            await db.SaveChangesAsync();

            db.Notifications.RemoveRange(db.Notifications.Where(f => f.Sender == currentUserId && f.Receiver == user.Id));
            await db.SaveChangesAsync();

            NotificationHub hub = new NotificationHub();
            await hub.Static_FetchNotification(id);

            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
