﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using WatchMe.Hubs;

[assembly: OwinStartupAttribute(typeof(WatchMe.Startup))]
namespace WatchMe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var idProvider = new CustomUserIdProvider();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);
            app.MapSignalR();
        }
    }
}
