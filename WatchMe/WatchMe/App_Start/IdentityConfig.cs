﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using WatchMe.Models;

namespace WatchMe
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }



    }

    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var manager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>() as ApplicationDbContext));

            return manager;
        }
    }

    //This is useful if you do not want to tear down the database each time you run the application.
    public sealed class ApplicationDbInitializer : IDisposable
    {
        private static readonly object ThisLock = new object();
        private static volatile ApplicationDbInitializer _applicationDbInitializer;

        private bool _isInitialized;

        private ApplicationDbInitializer(IOwinContext context)
        {
            Seed(context);
        }

        public static ApplicationDbInitializer Create(IdentityFactoryOptions<ApplicationDbInitializer> options, IOwinContext context)
        {
            if (_applicationDbInitializer != null)
                return _applicationDbInitializer;

            lock (ThisLock)
            {
                if (_applicationDbInitializer != null) return _applicationDbInitializer;
                _applicationDbInitializer = new ApplicationDbInitializer(context);
            }
            return _applicationDbInitializer;
        }

        /// <summary>
        /// Run Once Initialize code blocks.
        /// </summary>
        /// <param name="context"></param>
        private void Seed(IOwinContext context)
        {
            if (_isInitialized) return;

            InitializeDb(context);
            InitializeIdentity(context);

            _isInitialized = true;
        }

        // Verify Db or Tables and Create it, If you need.
        private static void InitializeDb(IOwinContext context)
        {
            // var oracleDatabase = context.Get<ApplicationDbContext>() as OracleDatabase;
            // e.g. Run the DDL if Table is not.
        }

        // Create Sample Admin User: admin@admin.com with password: Admin@123456 in the Admin role
        private static void InitializeIdentity(IOwinContext context)
        {
            var userManager = context.GetUserManager<ApplicationUserManager>();
            var roleManager = context.Get<ApplicationRoleManager>();

            const string name = "admin@admin.com";
            const string password = "Admin@123456";
            const string admin_r = "Admin";
            const string user_r = "User";
            //Create Role Admin if it does not exist
            var role = roleManager.FindByName(admin_r);
            if (role == null)
            {
                role = new IdentityRole(admin_r);
                var roleresult = roleManager.Create(role);
            }

            var userRole = roleManager.FindByName(user_r);
            if (userRole == null)
            {
                userRole = new IdentityRole(user_r);
                var userRoleresult = roleManager.Create(userRole);
            }

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name, SignUpDate = DateTime.Now, DateOfBirth = DateTime.Now };
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }

        bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                // Free any other managed objects here.
            }

            // Free any unmanaged objects here.
            _disposed = true;
        }
    }


}
