﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WatchMe
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        
        protected void Session_Start(object sender, EventArgs e)
        {
            GlobalFilters.Filters.Add(new TrackLoginsFilter());
        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && SecurityHelper.GetLoggedInUsers().ContainsKey(HttpContext.Current.User.Identity.GetUserId()))
            {
                SecurityHelper.GetLoggedInUsers().Remove(HttpContext.Current.User.Identity.GetUserId());
            }
        }

        public class TrackLoginsFilter : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                Dictionary<string, DateTime> loggedInUsers = SecurityHelper.GetLoggedInUsers();

                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (loggedInUsers.ContainsKey(HttpContext.Current.User.Identity.GetUserId()))
                    {
                        loggedInUsers[HttpContext.Current.User.Identity.GetUserId()] = System.DateTime.Now;
                    }
                    else
                    {
                        loggedInUsers.Add(HttpContext.Current.User.Identity.GetUserId(), System.DateTime.Now);
                    }

                }

                // remove users where time exceeds session timeout
                var keys = loggedInUsers.Where(u => DateTime.Now.Subtract(u.Value).Minutes >
                           HttpContext.Current.Session.Timeout).Select(u => u.Key);
                foreach (var key in keys)
                {
                    loggedInUsers.Remove(key);
                }

            }
        }

        public static class SecurityHelper
        {
            public static Dictionary<string, DateTime> GetLoggedInUsers()
            {
                Dictionary<string, DateTime> loggedInUsers = new Dictionary<string, DateTime>();

                if (HttpContext.Current != null)
                {
                    loggedInUsers = (Dictionary<string, DateTime>)HttpContext.Current.Application["loggedinusers"];
                    if (loggedInUsers == null)
                    {
                        loggedInUsers = new Dictionary<string, DateTime>();
                        HttpContext.Current.Application["loggedinusers"] = loggedInUsers;
                    }
                }
                return loggedInUsers;

            }
        }
    }

}
